import { createBottomTabNavigator, createAppContainer } from 'react-navigation';

import Main from './pages/main';
import Product from './pages/product';
import Photo from './pages/photo';


const TabNavigator = createBottomTabNavigator({
  Home: Main,
  Photo: Photo,
  Product: Product
});

export default createAppContainer(TabNavigator);