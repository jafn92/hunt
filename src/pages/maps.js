import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
// import Mapbox from '@mapbox/react-native-mapbox-gl';

// Mapbox.setAccessToken('pk.eyJ1Ijoiam9hcXVpbWFmbiIsImEiOiJjanE1OGtldmYyMjZwNDJ1aXN1eWpnYXRzIn0.YYkEGJjNozVkVpK3TuuXpQ');

export default class Maps extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Mapbox.MapView
                    styleURL={Mapbox.StyleURL.Dark}
                    zoomLevel={15}
                    centerCoordinate={[-38.50969791, -3.824112]}
                    style={styles.container}>
                </Mapbox.MapView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
  });