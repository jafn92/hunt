import { createDrawerNavigator } from 'react-navigation';
import Main from '../pages/main';
import Product from '../pages/product';


const DrawerNavigator = createDrawerNavigator({
  Home: Main,
  Products: Product,
});

export default { DrawerNavigator };